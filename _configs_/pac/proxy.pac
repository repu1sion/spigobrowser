//pactester -p ~/pacparser-master/tests/proxy.pac -u http://www.mozilla.org
//This command passes the host parameter www.mozilla.org and the url parameter http://www.mozilla.org

//extend this array with needed hosts
var whitelist = [
  "spigo.com",
  "spigo.co.uk",
  "spigo.dk"
];


function FindProxyForURL(url, host) {
	// loop here through array of hosts. if found - return "DIRECT". else return "PROXY 0" by default  
	for (var i = 0; i < whitelist.length; i++) 
	{
		if (shExpMatch(host, whitelist[i]))
		{
			return "DIRECT";
		}
	}

	// don't allow
	return "PROXY 0.0.0.0:8080"
}

//XXX - do we need it or not?
ret = FindProxyForURL(url, host);
